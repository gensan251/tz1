##
## shell.py - the weeman main shell
##
## Written by @Hypsurus 
##


import sys
import os
from core.complete import complete
from core.complete import array 
from core.config import __version__
from core.config import __codename__
from core.misc import printt
from core.misc import print_help
from core.config import url
from core.config import action_url
from core.config import port
from core.config import user_agent
from core.httpd import weeman

def print_startup():
    #print("\033[H\033[J") # Clear the screen
    print("\033[01;31m")
    print(open("core/logo.txt", "r").read())
    print("\033[00m")
    print("\033[01;33m\t    ..:: Weeman %s (%s) ::..\033[00m" %(__version__, __codename__)) 
    print("\033[01;34m\t-------------------------------------\033[00m")
    print("\t\'There are plenty of fish in the sea\'")
    print("\033[01;34m\t-------------------------------------\n\033[00m")
 
def shell():
    global url
    global port
    global action_url
    global user_agent

    print_startup()
    complete(array)

    if os.path.exists("history.log"):
        if  os.stat("history.log").st_size == 0:
            history = open("history.log", "w")
        else:
            history = open("history.log", "a")
    else:
        history = open("history.log", "w")

    while True:
        try:
            if sys.argv[1] == "run":
                s = weeman(url,port)
                s.clone()
                s.serve()
            else:
                print("Error: What? try pass arg run.")

        except KeyboardInterrupt:
            s = weeman(url,port)
            s.cleanup()
            print("\nInterrupt ...")
            break
        except Exception as e:
            printt(3, "Error: Weeman recived error! (%s)" %(str(e)))
